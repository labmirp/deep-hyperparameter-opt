# Efficient Hyperparameter Optimization in Convolutional Neural Networks by Learning Curves Prediction #
  
  
Time series predictor for learning curves in neural networks using multiple iterative support vector regression.  
To make a reliable prediction we employed bootstrapping over the transition matrix(Hankel matrix), therefore, our predictor performs 200 different predictions over the learning curve using different versions of the Hankel matrix.
  
### Methodology ###
The following steps are performed to make a prediction:  
  
1) Smoothing of the learning curve using Singular Spectrum analysis (Eigen-values are selected automatically)  
2) Hyperparameter optimization of the multiple iterated SVRs by the Sine-Cosine algorithm  
3) Learning Curves prediction using Bootstrapping  
4) Finally we select the median of the final values to estimate the final performance  
  
### Requirements ###
The following packages are required to use our method:  
  
1) Numpy  
2) Scipy  
3) Scikit learn  
4) Itertools  
5) Multiprocessing  
6) Functools  
  
### Please cite ###
  
This work is an improvement of the following paper:  
  
Cardona-Escobar, A. F., Giraldo-Forero, A. F., Castro-Ospina, A. E., & Jaramillo-Garzon, J. A. (2017, November). Efficient Hyperparameter Optimization in Convolutional Neural Networks by Learning Curves Prediction. In Iberoamerican Congress on Pattern Recognition (pp. 143-151). Springer, Cham.

### Instructions of use ###

1) Clone the repository:  
git clone https://bitbucket.org/labmirp/deep-hyperparameter-opt.git
  
2) Copy the files:  
  
Learning_curve.py  
fold_cross_validation.py  
MSVR_prediction.py  
SSA.py  
  
to your current project  
  
3) import the file Learning curve as follows:  
from Learning_curve import learning_curve  
  
### Example ###

```python
#Example of use of the learning curves prediction method
from Learning_curve import learning_curve

import matplotlib.pyplot as plt
import numpy as np

#Creating a synthetic learning curve to predict
a = 1-np.exp(-np.arange(0,50,0.03)/8)
a = a + np.random.normal(0,0.05*np.std(a),len(a))

#Smoothing and Prediction
v = learning_curve(a[:300])
v.smooth_ssa()
d = v.predict_curve(h = 1666,kernel='linear',folds=5,g=2)

#Plot the final predictions
plt.plot(a)
datos = list()
for i in range(200):
	plt.plot(d[i])
	datos.append(d[i][-1])

plt.show()

#Computing the final value using the median as estimator
m = np.median(datos)
#Prediction interval
inter = 1.96*np.std(datos)/np.sqrt(200)

```
  
  
### Example ###
  
After runing the example the result will be similar to the right part of the following picture
  
![picture](img/result.png)

