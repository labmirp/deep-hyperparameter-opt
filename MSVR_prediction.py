import numpy as np
import scipy
from sklearn.svm import SVR
import copy
#Prediction of learning curve by using iterated SVRs (lineal kernel)
#Inputs:
#additional: list(learning_curve, horizon_to_predict, number_of_SVRs, kernel) , optimal_parameters

#Outputs:
#y_new : Predicted Curve

def MSVR_prediction(additional,opt):
	
	x = additional[0]#first values
	H = additional[1]#horizon for prediction
	kern = additional[2]#type of kernel
	interval = additional[3]
	
	#optimal parameters
	C = opt[0]
	eps = opt[1]
	gam = opt[2]
	T = int(opt[3])#number of machines
	d = int(opt[4])#size of slicing window
	
	r = (H-len(x))//T #number of values to predict
	
	# Hankel matrix generation
	#d window size
	#T Number of SVR models
	N = len(x) #number of samples
	y_series = np. zeros([N-d+1,d])# Creating variable for Hankel matrix
	for i in range(len(y_series)):
		
		for j in range(d):
			
			y_series[i][j] = x[i+j]
			
		
	
	HAN = copy.deepcopy(y_series)
	LAB = copy.deepcopy(x[d:])
	
	evt = 0
	envolvent = list()
	while evt < interval:
		
		evt = evt + 1
		
		T_models = list() # Stroraging the T models
		for i in range(T):
			
			#computing labels
			l = LAB[i:]
			XX = y_series[0:len(l),:]
			
			if evt > 0:
				samp = np.random.choice(len(l), len(l), True)
				XX = XX[samp]
				l = l[samp]
			
			#training models
			T_models.append( SVR(C=C, epsilon=eps, kernel=kern, gamma = gam, max_iter=500) )
			T_models[-1].fit(XX, l)
			
		
		#vector to save predictions and the initial part of the curve which is x
		y_new = copy.deepcopy(x)
		
		loop = 1
		while loop <= r:
			
			#Prediction of new horizons
			a = np.reshape(copy.deepcopy(y_new[-d:]),[1,-1])
			for i in range(T):
				
				l = x[(d+i):N]
				y_new = np.append( y_new, T_models[i].predict(a) )
				
			
			loop = loop + 1
			
		
		envolvent.append( y_new )
		
		
	
	return( np.stack(envolvent) )
	

