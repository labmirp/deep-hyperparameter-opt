import numpy as np
import itertools
import copy
from fold_cross_validation import fold_cross_val
from fold_cross_validation import parallel_val
from MSVR_prediction import MSVR_prediction
from SSA import SSA


class learning_curve():
	
	def __init__(self, x):
		
		self.x = x#learning curve observations
		self.n =len(x)#learning curve size
		self.pred = x#predicted curve
		
	
	#Method for learning curve smoothing
	def smooth_ssa(self):
		
		s = len(self.x)/2#window size in SSA
		self.x = SSA(self.x,s)# SSA for learning curve smoothing
		
	
	#This function optimizes the cross validation of learning curves prediction
	def SCA_optimization(self, obj_func, lower, upper, additional, agents = 30, iterations = 100, a = 5):
		
		#Computing number of parameters
		n_par = len(lower)
		
		#Initializing solutions
		X = np.random.uniform(lower,upper,(agents,n_par))
		Y = obj_func( [self.x, additional[0], additional[1], 'linear'], X, 10 )
		
		p = copy.deepcopy(X[np.argmin(Y)])
		py = min(Y)
		
		#Matrices for limiting bounds
		lowermat = np.outer(np.ones(agents),lower)
		uppermat = np.outer(np.ones(agents),upper)
		
		for i in range(iterations):
			#updating r1
			r1 = a-(i+2)*1.0*a/iterations
			
			#updating agents
			for j in range(agents):
				for k in range(n_par):
					
					r2 = np.random.uniform(0,2*np.pi)
					r5 = np.random.uniform(0,2*np.pi)
					
					r3 = np.random.uniform(0,2)
					r4 = np.random.uniform(0,1)
					
					if r4 < 0.5:
						
						X[j,k] = X[j,k] + r1*np.sin(r2)*np.abs(r3*p[k] - X[j,k])
						
					
					elif r4 >= 0.5:
						
						X[j,k] = X[j,k] + r1*np.cos(r2)*np.abs(r3*p[k] - X[j,k])
						
					
					
				
			
			#Limiting bounds
			X[X > uppermat] = uppermat[X > uppermat]
			X[X < lowermat] = lowermat[X < lowermat]
			
			#Evaluating new agents
			Y = obj_func( [self.x, additional[0], additional[1], 'linear'], X, 10 )
			
			if np.min(Y) < py:
				
				p = copy.deepcopy(X[np.argmin(Y)])
				py = np.min(Y)
				
				#print(p)
				
			
		
		return p
		
	
	#Method for learning curves prediction
	# h is the prediction horizon
	# T is the Number of machines
	# d is the window size in MSVR
	# Folds number of folds in cross validation
	# g is the number of points to predict for each machine on each fold
	def predict_curve(self,h,kernel='linear',folds=4,g=2):
		
		#Grid_search optimization
		epsilon=10.**np.arange(-7,-1)
		C=10.**np.arange(-1,7)
		gamma=2.**np.arange(-6,-1)
		#possible hyperparameter combinations
		par = np.array(tuple(itertools.product(*[C,epsilon,gamma])))
		
		#gs1 = parallel_val( [self.x,folds,T,d,kernel,g], par, 10 )
		#par1 = par[np.argmin(gs1)]
		
		#C,eps,gamma,T,d
		lower = [0.1,1.e-7,2e-6,2,2]
		upper = [1.e7,0.1,0.5,10,10]
		agents = 35
		iterations = 500
		
		par1 = self.SCA_optimization(parallel_val, lower, upper, [folds, g], agents, iterations, 5)
		self.pred = MSVR_prediction([self.x,h,kernel,200], par1)
		
		return self.pred
		
	
	













