#Example of SSA
import numpy as np
#Singular Spectrum analysis using SVT Threshold
#(The Optimal Hard Threshold for Singular Values is 4/sqrt(3))

#Input parameters:
#x : learning curve
#d : window size (normally len(x)/2)

#Outputs:
#Ch : Filtered curve
def SSA(x,d):
	
	aprox = np.arange(0.05,1.01,0.05)
	
	#values computed in: Gavish, M., & Donoho, D. L. (2014). The optimal hard threshold for singular values is $4/\sqrt {3} $. IEEE Transactions on Information Theory, 60(8), 5040-5053.
	#table IV (unkwon level of noise)
	trh = np.array([1.5194,1.6089,1.6896,1.7650,1.8371,1.9061,1.9741,2.0403,2.106,2.1711,2.2365,2.3021,2.3679,2.4339,2.5011,2.5697,2.6399,2.7099,2.7832,2.8582])
	
	N = len(x) #number of samples
	Q = np. zeros([N-d+1,d])# Creating variable for Hankel matrix
	for i in range(len(Q)):
		
		for j in range(d):
			
			Q[i][j] = x[i+j]
			
		
	
	#SVD for covariance matrix
	U, s, _ = np.linalg.svd(Q, full_matrices=False)
	rank = np.linalg.matrix_rank(Q)
	
	#Relation among rows and cols
	beta_r = min(1.0*Q.shape[0]/Q.shape[1],1.0*Q.shape[1]/Q.shape[0])
	beta = np.argmin(np.abs(beta_r-aprox))
	
	V = np.zeros([d,N-d+1])#Computed based on U (SSA algorithm)
	E = np.zeros(Q.shape)#Approximated Hankel matrix
	
	y_median = np.median(s)#median of eigenvalues
	for i in range(rank):
		
		if s[i] > trh[beta]*y_median:
			
			V[:,i] = np.matmul(np.transpose(Q),U[:,i]/s[i])
			E = E + s[i]*np.outer(U[:,i],V[:,i])
			
		
	
	#antidiagonal averaging of E
	H = np.rot90(E,k=1)
	Ch = np.zeros([N])
	for k in range(-(H.shape[0]-1),H.shape[1]):
		
		Ch[k] = np.mean(H.diagonal(k))
		
	
	Ch = np.concatenate([Ch[-d+1:],Ch[:N-d]])
	
	return(Ch)
	
