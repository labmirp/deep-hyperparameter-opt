#Example of use of the learning curves prediction method
from Learning_curve import learning_curve

import matplotlib.pyplot as plt
import numpy as np

#Creating a synthetic learning curve to predict
a = 1-np.exp(-np.arange(0,50,0.03)/8)
a = a + np.random.normal(0,0.05*np.std(a),len(a))

#Smoothing and Prediction
v = learning_curve(a[:300])
v.smooth_ssa()
d = v.predict_curve(h = 1666,kernel='linear',folds=5,g=2)

#Plot the final predictions
plt.plot(a)
datos = list()
for i in range(200):
	plt.plot(d[i])
	datos.append(d[i][-1])

plt.show()

#Computing the final value using the median as estimator
m = np.median(datos)
#Prediction interval
inter = 1.96*np.std(datos)/np.sqrt(200)
