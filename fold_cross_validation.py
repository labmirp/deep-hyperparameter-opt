import itertools
import numpy as np
from multiprocessing import Pool
from functools import partial
from sklearn.svm import SVR
import copy
#K-fold cross-validation for time series usign iterated SVR
#Paper : Zhang, L., Zhou, W. D., Chang, P. C., Yang, J. W., & Li, F. Z. (2013). Iterated time series prediction with multiple support vector regression models. Neurocomputing, 99, 411-422.

#Inputs:
#additional : list object with 4 arguments
#list(learning_curve, number_of_folds, number_of_points to adjust, kernel type)
#
#opt : list(C,epsilon) all svr models have the same parameters

#Outputs
#tmse : sum of errors

def fold_cross_val(additional, opt):
	
	#aditional parameters
	x = additional[0]#data
	folds = additional[1]#folds in cross validation
	g = additional[2]#number of points to adjust
	kern = additional[3]#type of kernel
	
	#current hyperparameters
	C = opt[0]
	eps = opt[1]
	gam = opt[2]
	T = int(opt[3])#number of svr models
	d = int(opt[4])#window size
	
	# Transition matrix generation
	# d window size
	# T Number of SVR models
	N = len(x) #number of observations in time series
	y_series = np.zeros([N-d+1,d])# Creating variable for Hankel matrix
	for i in range(len(y_series)):# Creating hankel matrix
		
		for j in range(d):
			
			y_series[i][j] = x[i+j]
			
		
	
	#number of total samples in transition matrix = (N-d+1)
	total = y_series.shape[0]
	
	#For each fold in cross validation
	mse = list()
	for f in range(folds):
		
		#Taking train data in fold f
		#each svr predicts g samples
		
		#total-(fold * number of points to evaluate * number of machines)
		index = np.array(range(total-(folds-f)*g*T))
		
		#print(index)
		train = y_series[index]
		total_fold = len(train)#total of training samples in fold f
		
		# For each svr perform training
		T_models = list() # Stroraging models
		for i in range(T):
			
			#computing labels
			l = x[(d+i):d+total_fold-1]
			
			T_models.append( SVR(C=C, epsilon=eps, kernel=kern, gamma=gam, max_iter=500) )
			T_models[-1].fit(train[:len(l),:], l)
			
		
		
		#Construct until last sample
		index = np.array(range(N-(folds-f)*g*T))
		y_new = copy.deepcopy(x[index])
		
		#Prediction of new horizons
		for j in range(g):
			
			#Last sample
			a = np.reshape(copy.deepcopy(y_new[-d:]),[1,-1])
			for i in range(T):
				
				l = x[(d+i):d+total_fold-1]
				y_new = np.append( y_new, T_models[i].predict(a) )
				
			
		
		mse.append( np.mean( (x[0:N-folds*g*T+(f+1)*T*g]-y_new)**2.0 ) )
		
	
	tmse = np.mean( np.array(mse) )
	
	#print("Validation Completed, MSE: ", tmse)
	
	return( tmse )
	


#parallelization of cross_validation
def parallel_val(a,o,cores):
	
	pool = Pool(processes=cores)
	MSE = pool.map(partial(fold_cross_val, a), o)
	pool.close()
	
	return(np.asarray(MSE))
	



##Example of use of the learning curves prediction method
#from Learning_curve import learning_curve

#import matplotlib.pyplot as plt
#import numpy as np

#a = 1-np.exp(-np.arange(0,50,0.03)/8) 
#a = a + np.random.normal(0,0.05*np.std(a),len(a))

#SCA_optimization(parallel_val, [0.001,0.001,0.001], [1000.,0.1,1.], a, 10, 30, 5)


#plt.plot(a)
#plt.plot(d)
#plt.show()








